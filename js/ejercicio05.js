function ejercicio05(arreglo) {
    var resultado=[];
    var suma=0;
    for(var i=0; i<arreglo.length;i++){
        suma=0;
        for(var j=0;j<arreglo[i].length;j++){
            suma+=arreglo[i][j];
        }
        resultado.push(suma);
    }
    return resultado;
}
